@extends('adminlte::page')

@section('title', 'AgendaL6')

@section('content_header')
<h1>Criação de um novo Contato</h1>
@stop

@section('content')
<form action="{{ route('agenda.store') }}" method="POST">
    {{csrf_field()}}
    <div class="panel panel-default">
        <div class="panel-heading">
            Cadastro de novos contatos
        </div>

        <div class="panel-body">
            <div class="form-group">
                <label for="nome">Nome do Contato</label>
                <input type="text" id="nome" name="nome" class="form-control">
            </div>

            <div class="form-group">
                <label for="fone_res">Telefone Residencial</label>
                <input type="text" id="fone_res" name="fone_res" class="form-control">
            </div>

            <div class="form-group">
                <label for="fone_cel">Telefone Celular</label>
                <input type="text" id="fone_cel" name="fone_cel" class="form-control">
            </div>

            <div class="form-group">
                <label for="dt_nasc">Data de Nascimento</label>
                <input type="date" id="dt_nasc" name="dt_nasc" class="form-control">
            </div>

        <div class="form-group">
                <label for="facebook">facebook</label>
                <input type="text" id="facebook" name="facebook" class="form-control">
            </div>

          <div class="form-group">
                <label for="instagram">instagram</label>
                <input type="text" id="instagram" name="instagram" class="form-control">
    
            </div>
                    
            <div class="form-group">
                <label for="twitter">twitter</label>
                <input type="text" id="twitter" name="twitter" class="form-control">
    
            </div>


        </div>

        <div class="panel-footer">
        <a href= "{{ route('agenda.index')}}" class="btn btn-defalt">
        <i class= "fas fa-reply"></i> voltar
        </a>

        <buttun type= "submit" class= "btn btn-sucess">
        <i class= "fas fa-save"><</i> Gravar
                </buttun>

        </div>
    </div>

</form>
@stop

@section('css')
@stop

@section('js')
@stop
